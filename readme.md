# SteamVR sRGB Bug Demo

A demonstration of a bug in SteamVR where swapchain images supplied to SteamVR via OpenXR have an unnecessary linear-to-sRGB conversion function applied to them. The result is that colors are significantly off from their intended values.

## Usage

1. Build with `./build.sh`.
2. Run with `./demo uncorrected`. You will see a bright blue color in both the "VR View" window of SteamVR on your desktop and in the headset itself. This color is incorrect.
3. Close the program and re-run with `./demo corrected`. You will see a much darker blue color in both places. This color is correct.

## Explanation

The program draws a solid color to the screen, the chosen color is `#002b36` from the Solarized color scheme. It should look like this:

![](demo.png)

The program draws the color by setting it as the clear value for a Vulkan render pass which targets the swapchain image supplied by OpenXR. This means the color is written directly into the pixels of the swapchain image, if the swapchain image is in an sRGB format (which by default it is) a linear-to-sRGB conversion function is applied by the graphics driver before writing. This sRGB-encoded color is then passed to SteamVR. SteamVR applies an **unnecessary linear-to-sRGB conversion function** to the image which results in a massive increase in brightness of all colors that weren't already at minimum or maximum brightness (e.g. `#ffffff`, `#ff0000` or `#000000` will be unaffected).

When the program is started with the `corrected` flag it applies a deliberately nonsensical sRGB-to-linear conversion function to the already linear color value (see line 1081). This counteracts the unnecessary conversion inside of SteamVR and results in the correct color reaching the display.

If the program instead used a linear color format (e.g. by modifying line 559 to look for `VK_FORMAT_R16G16B16A16_SFLOAT` or `VK_FORMAT_R32G32B32A32_SFLOAT` instead) this problem will still occur and the behavior is exactly the same. This suggests regardless of the code path used inside of SteamVR there is always one extra unnecessary linear-to-sRGB conversion function applied i.e. the issue is not SteamVR treating sRGB swapchain images as linear, but SteamVR handling all OpenXR swapchain images incorrectly.

## Credit

Thanks to [this post](https://steamcommunity.com/app/250820/discussions/8/3194742149932987233/) for helping me figure out what was going on here.
