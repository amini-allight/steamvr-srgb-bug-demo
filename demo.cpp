#include <vulkan/vulkan.h>
#define XR_USE_GRAPHICS_API_VULKAN
#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>

#include <iostream>
#include <cstring>
#include <csignal>
#include <tuple>
#include <set>
#include <vector>
#include <fstream>
#include <streambuf>
#include <limits>
#include <cmath>

using namespace std;

static const char* const applicationName = "SteamVR sRGB Bug Demo";
static const unsigned int majorVersion = 0;
static const unsigned int minorVersion = 1;
static const unsigned int patchVersion = 0;
static const char* const layerNames[] = { "XR_APILAYER_LUNARG_core_validation" };
static const char* const extensionNames[] = {
    "XR_KHR_vulkan_enable",
    "XR_KHR_vulkan_enable2",
    "XR_EXT_debug_utils"
};
static const char* const vulkanLayerNames[] = { "VK_LAYER_KHRONOS_validation" };
static const char* const vulkanExtensionNames[] = { "VK_EXT_debug_utils" };

static const size_t bufferSize = sizeof(float) * 4 * 4 * 3;

static const size_t eyeCount = 2;

static const float nearDistance = 0.01;
static const float farDistance = 1'000;

static const float grabDistance = 1;

static bool quit = false;

static bool corrected = false;

static int objectGrabbed = 0;
static XrVector3f objectPos = { 0, 0, 0 };

static float srgbToLinear(float srgb)
{
    if (srgb < 0.04045)
    {
        return srgb / 12.92;
    }
    else
    {
        return pow((srgb + 0.055) / 1.055, 2.4);
    }
}

struct Swapchain
{
    Swapchain(XrSwapchain swapchain, VkFormat format, uint32_t width, uint32_t height)
        : swapchain(swapchain)
        , format(format)
        , width(width)
        , height(height)
    {

    }

    ~Swapchain()
    {
        xrDestroySwapchain(swapchain);
    }

    XrSwapchain swapchain;
    VkFormat format;
    uint32_t width;
    uint32_t height;
};

struct SwapchainImage
{
    SwapchainImage(
        VkPhysicalDevice physicalDevice,
        VkDevice device,
        VkRenderPass renderPass,
        VkCommandPool commandPool,
        const Swapchain* swapchain,
        XrSwapchainImageVulkanKHR image
    )
        : device(device)
        , commandPool(commandPool)
        , image(image)
    {
        VkImageViewCreateInfo imageViewCreateInfo{};
        imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewCreateInfo.image = image.image;
        imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        imageViewCreateInfo.format = swapchain->format;
        imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
        imageViewCreateInfo.subresourceRange.levelCount = 1;
        imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
        imageViewCreateInfo.subresourceRange.layerCount = 1;

        VkResult result = vkCreateImageView(device, &imageViewCreateInfo, nullptr, &imageView);

        if (result != VK_SUCCESS)
        {
            cerr << "Failed to create Vulkan image view: " << result << endl;
        }

        VkFramebufferCreateInfo framebufferCreateInfo{};
        framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferCreateInfo.renderPass = renderPass;
        framebufferCreateInfo.attachmentCount = 1;
        framebufferCreateInfo.pAttachments = &imageView;
        framebufferCreateInfo.width = swapchain->width;
        framebufferCreateInfo.height = swapchain->height;
        framebufferCreateInfo.layers = 1;

        result = vkCreateFramebuffer(device, &framebufferCreateInfo, nullptr, &framebuffer);

        if (result != VK_SUCCESS)
        {
            cerr << "Failed to create Vulkan framebuffer: " << result << endl;
        }

        VkCommandBufferAllocateInfo commandBufferAllocateInfo{};
        commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAllocateInfo.commandPool = commandPool;
        commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAllocateInfo.commandBufferCount = 1;

        result = vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &commandBuffer);

        if (result != VK_SUCCESS)
        {
            cerr << "Failed to allocate Vulkan command buffers: " << result << endl;
        }
    }

    ~SwapchainImage()
    {
        vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
        vkDestroyFramebuffer(device, framebuffer, nullptr);
        vkDestroyImageView(device, imageView, nullptr);
    }

    XrSwapchainImageVulkanKHR image;
    VkImageView imageView;
    VkFramebuffer framebuffer;
    VkCommandBuffer commandBuffer;

private:
    VkDevice device;
    VkCommandPool commandPool;
};

PFN_xrVoidFunction getXRFunction(XrInstance instance, const char* name)
{
    PFN_xrVoidFunction func;

    XrResult result = xrGetInstanceProcAddr(instance, name, &func);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to load OpenXR extension function '" << name << "': " << result << endl;
        return nullptr;
    }

    return func;
}

PFN_vkVoidFunction getVKFunction(VkInstance instance, const char* name)
{
    PFN_vkVoidFunction func = vkGetInstanceProcAddr(instance, name);

    if (!func)
    {
        cerr << "Failed to load Vulkan extension function '" << name << "'." << endl;
        return nullptr;
    }

    return func;
}

XrBool32 handleXRError(
    XrDebugUtilsMessageSeverityFlagsEXT severity,
    XrDebugUtilsMessageTypeFlagsEXT type,
    const XrDebugUtilsMessengerCallbackDataEXT* callbackData,
    void* userData
)
{
    cerr << "OpenXR ";

    switch (type)
    {
    case XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT :
        cerr << "general ";
        break;
    case XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT :
        cerr << "validation ";
        break;
    case XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT :
        cerr << "performance ";
        break;
    case XR_DEBUG_UTILS_MESSAGE_TYPE_CONFORMANCE_BIT_EXT :
        cerr << "conformance ";
        break;
    }

    switch (severity)
    {
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT :
        cerr << "(verbose): ";
        break;
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT :
        cerr << "(info): ";
        break;
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT :
        cerr << "(warning): ";
        break;
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT :
        cerr << "(error): ";
        break;
    }

    cerr << callbackData->message << endl;

    return XR_FALSE;
}

VkBool32 handleVKError(
    VkDebugUtilsMessageSeverityFlagBitsEXT severity,
    VkDebugUtilsMessageTypeFlagsEXT type,
    const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
    void* userData
)
{
    cerr << "Vulkan ";

    switch (type)
    {
    case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT :
        cerr << "general ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT :
        cerr << "validation ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT :
        cerr << "performance ";
        break;
    }

    switch (severity)
    {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT :
        cerr << "(verbose): ";
        break;
    default :
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT :
        cerr << "(info): ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT :
        cerr << "(warning): ";
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT :
        cerr << "(error): ";
        break;
    }

    cerr << callbackData->pMessage << endl;

    return VK_FALSE;
}

XrInstance createInstance()
{
    XrInstance instance;

    XrInstanceCreateInfo instanceCreateInfo{};
    instanceCreateInfo.type = XR_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.createFlags = 0;
    strcpy(instanceCreateInfo.applicationInfo.applicationName, applicationName);
    instanceCreateInfo.applicationInfo.applicationVersion = XR_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    strcpy(instanceCreateInfo.applicationInfo.engineName, applicationName);
    instanceCreateInfo.applicationInfo.engineVersion = XR_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    instanceCreateInfo.applicationInfo.apiVersion = XR_CURRENT_API_VERSION;
    instanceCreateInfo.enabledApiLayerCount = 1;
    instanceCreateInfo.enabledApiLayerNames = layerNames;
    instanceCreateInfo.enabledExtensionCount = sizeof(extensionNames) / sizeof(const char*);
    instanceCreateInfo.enabledExtensionNames = extensionNames;

    XrResult result = xrCreateInstance(&instanceCreateInfo, &instance);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create OpenXR instance: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return instance;
}

void destroyInstance(XrInstance instance)
{
    xrDestroyInstance(instance);
}

XrDebugUtilsMessengerEXT createDebugMessenger(XrInstance instance)
{
    XrDebugUtilsMessengerEXT debugMessenger;

    XrDebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo{};
    debugMessengerCreateInfo.type = XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    debugMessengerCreateInfo.messageSeverities = XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debugMessengerCreateInfo.messageTypes = XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_TYPE_CONFORMANCE_BIT_EXT;
    debugMessengerCreateInfo.userCallback = handleXRError;
    debugMessengerCreateInfo.userData = nullptr;

    auto xrCreateDebugUtilsMessengerEXT = (PFN_xrCreateDebugUtilsMessengerEXT)getXRFunction(instance, "xrCreateDebugUtilsMessengerEXT");

    XrResult result = xrCreateDebugUtilsMessengerEXT(instance, &debugMessengerCreateInfo, &debugMessenger);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create OpenXR debug messenger: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return debugMessenger;
}

void destroyDebugMessenger(XrInstance instance, XrDebugUtilsMessengerEXT debugMessenger)
{
    auto xrDestroyDebugUtilsMessengerEXT = (PFN_xrDestroyDebugUtilsMessengerEXT)getXRFunction(instance, "xrDestroyDebugUtilsMessengerEXT");

    xrDestroyDebugUtilsMessengerEXT(debugMessenger);
}

XrSystemId getSystem(XrInstance instance)
{
    XrSystemId systemID;

    XrSystemGetInfo systemGetInfo{};
    systemGetInfo.type = XR_TYPE_SYSTEM_GET_INFO;
    systemGetInfo.formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;

    XrResult result = xrGetSystem(instance, &systemGetInfo, &systemID);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get system: " << result << endl;
        return XR_NULL_SYSTEM_ID;
    }

    return systemID;
}

tuple<XrGraphicsRequirementsVulkanKHR, set<string>> getVulkanInstanceRequirements(XrInstance instance, XrSystemId system)
{
    auto xrGetVulkanGraphicsRequirementsKHR = (PFN_xrGetVulkanGraphicsRequirementsKHR)getXRFunction(instance, "xrGetVulkanGraphicsRequirementsKHR");
    auto xrGetVulkanInstanceExtensionsKHR = (PFN_xrGetVulkanInstanceExtensionsKHR)getXRFunction(instance, "xrGetVulkanInstanceExtensionsKHR");

    XrGraphicsRequirementsVulkanKHR graphicsRequirements{};
    graphicsRequirements.type = XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR;

    XrResult result = xrGetVulkanGraphicsRequirementsKHR(instance, system, &graphicsRequirements);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan graphics requirements: " << result << endl;
        return { graphicsRequirements, {} };
    }

    uint32_t instanceExtensionsSize;

    result = xrGetVulkanInstanceExtensionsKHR(instance, system, 0, &instanceExtensionsSize, nullptr);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan instance extensions: " << result << endl;
        return { graphicsRequirements, {} };
    }

    char* instanceExtensionsData = new char[instanceExtensionsSize];

    result = xrGetVulkanInstanceExtensionsKHR(instance, system, instanceExtensionsSize, &instanceExtensionsSize, instanceExtensionsData);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan instance extensions: " << result << endl;
        return { graphicsRequirements, {} };
    }

    set<string> instanceExtensions;

    uint32_t last = 0;
    for (uint32_t i = 0; i <= instanceExtensionsSize; i++)
    {
        if (i == instanceExtensionsSize || instanceExtensionsData[i] == ' ')
        {
            instanceExtensions.insert(string(instanceExtensionsData + last, i - last));
            last = i + 1;
        }
    }

    delete[] instanceExtensionsData;

    return { graphicsRequirements, instanceExtensions };
}

tuple<VkPhysicalDevice, set<string>> getVulkanDeviceRequirements(XrInstance instance, XrSystemId system, VkInstance vulkanInstance)
{
    auto xrGetVulkanGraphicsDeviceKHR = (PFN_xrGetVulkanGraphicsDeviceKHR)getXRFunction(instance, "xrGetVulkanGraphicsDeviceKHR");
    auto xrGetVulkanDeviceExtensionsKHR = (PFN_xrGetVulkanDeviceExtensionsKHR)getXRFunction(instance, "xrGetVulkanDeviceExtensionsKHR");

    VkPhysicalDevice physicalDevice;

    XrResult result = xrGetVulkanGraphicsDeviceKHR(instance, system, vulkanInstance, &physicalDevice);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan graphics device: " << result << endl;
        return { VK_NULL_HANDLE, {} };
    }

    uint32_t deviceExtensionsSize;

    result = xrGetVulkanDeviceExtensionsKHR(instance, system, 0, &deviceExtensionsSize, nullptr);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan device extensions: " << result << endl;
        return { VK_NULL_HANDLE, {} };
    }

    char* deviceExtensionsData = new char[deviceExtensionsSize];

    result = xrGetVulkanDeviceExtensionsKHR(instance, system, deviceExtensionsSize, &deviceExtensionsSize, deviceExtensionsData);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get Vulkan device extensions: " << result << endl;
        return { VK_NULL_HANDLE, {} };
    }

    set<string> deviceExtensions;

    uint32_t last = 0;
    for (uint32_t i = 0; i <= deviceExtensionsSize; i++)
    {
        if (i == deviceExtensionsSize || deviceExtensionsData[i] == ' ')
        {
            deviceExtensions.insert(string(deviceExtensionsData + last, i - last));
            last = i + 1;
        }
    }

    delete[] deviceExtensionsData;

    return { physicalDevice, deviceExtensions };
}

XrSession createSession(
    XrInstance instance,
    XrSystemId systemID,
    VkInstance vulkanInstance,
    VkPhysicalDevice physDevice,
    VkDevice device,
    uint32_t queueFamilyIndex
)
{
    XrSession session;

    XrGraphicsBindingVulkanKHR graphicsBinding{};
    graphicsBinding.type = XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR;
    graphicsBinding.instance = vulkanInstance;
    graphicsBinding.physicalDevice = physDevice;
    graphicsBinding.device = device;
    graphicsBinding.queueFamilyIndex = queueFamilyIndex;
    graphicsBinding.queueIndex = 0;

    XrSessionCreateInfo sessionCreateInfo{};
    sessionCreateInfo.type = XR_TYPE_SESSION_CREATE_INFO;
    sessionCreateInfo.next = &graphicsBinding;
    sessionCreateInfo.createFlags = 0;
    sessionCreateInfo.systemId = systemID;

    XrResult result = xrCreateSession(instance, &sessionCreateInfo, &session);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create OpenXR session: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return session;
}

void destroySession(XrSession session)
{
    xrDestroySession(session);
}

tuple<Swapchain*, Swapchain*> createSwapchains(XrInstance instance, XrSystemId system, XrSession session)
{
    uint32_t configViewsCount = eyeCount;
    vector<XrViewConfigurationView> configViews(
        configViewsCount,
        { .type = XR_TYPE_VIEW_CONFIGURATION_VIEW }
    );

    XrResult result = xrEnumerateViewConfigurationViews(
        instance,
        system,
        XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
        configViewsCount,
        &configViewsCount,
        configViews.data()
    );

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate view configuration views: " << result << endl;
        return { nullptr, nullptr };
    }

    uint32_t formatCount = 0;

    result = xrEnumerateSwapchainFormats(session, 0, &formatCount, nullptr);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate swapchain formats: " << result << endl;
        return { nullptr, nullptr };
    }

    vector<int64_t> formats(formatCount);

    result = xrEnumerateSwapchainFormats(session, formatCount, &formatCount, formats.data());

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate swapchain formats: " << result << endl;
        return { nullptr, nullptr };
    }

    int64_t chosenFormat = formats.front();

    for (int64_t format : formats)
    {
        if (format == VK_FORMAT_R8G8B8A8_SRGB)
        {
            chosenFormat = format;
            break;
        }
    }

    XrSwapchain swapchains[eyeCount];

    for (size_t i = 0; i < eyeCount; i++)
    {
        XrSwapchainCreateInfo swapchainCreateInfo{};
        swapchainCreateInfo.type = XR_TYPE_SWAPCHAIN_CREATE_INFO;
        swapchainCreateInfo.usageFlags = XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;
        swapchainCreateInfo.format = chosenFormat;
        swapchainCreateInfo.sampleCount = VK_SAMPLE_COUNT_1_BIT;
        swapchainCreateInfo.width = configViews[i].recommendedImageRectWidth;
        swapchainCreateInfo.height = configViews[i].recommendedImageRectHeight;
        swapchainCreateInfo.faceCount = 1;
        swapchainCreateInfo.arraySize = 1;
        swapchainCreateInfo.mipCount = 1;

        result = xrCreateSwapchain(session, &swapchainCreateInfo, &swapchains[i]);

        if (result != XR_SUCCESS)
        {
            cerr << "Failed to create swapchain: " << result << endl;
            return { nullptr, nullptr };
        }
    }

    return {
        new Swapchain(
            swapchains[0],
            (VkFormat)chosenFormat,
            configViews[0].recommendedImageRectWidth,
            configViews[0].recommendedImageRectHeight
        ),
        new Swapchain(
            swapchains[1],
            (VkFormat)chosenFormat,
            configViews[1].recommendedImageRectWidth,
            configViews[1].recommendedImageRectHeight
        )
    };
}

XrSpace createSpace(XrSession session)
{
    XrSpace space;

    XrReferenceSpaceCreateInfo spaceCreateInfo{};
    spaceCreateInfo.type = XR_TYPE_REFERENCE_SPACE_CREATE_INFO;
    spaceCreateInfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_STAGE;
    spaceCreateInfo.poseInReferenceSpace = { { 0, 0, 0, 1 }, { 0, 0, 0 } };

    XrResult result = xrCreateReferenceSpace(session, &spaceCreateInfo, &space);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create space: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return space;
}

void destroySpace(XrSpace space)
{
    xrDestroySpace(space);
}

XrActionSet createActionSet(XrInstance instance)
{
    XrActionSet actionSet;

    XrActionSetCreateInfo actionSetCreateInfo{};
    actionSetCreateInfo.type = XR_TYPE_ACTION_SET_CREATE_INFO;
    strcpy(actionSetCreateInfo.actionSetName, "openxr_example");
    strcpy(actionSetCreateInfo.localizedActionSetName, applicationName);
    actionSetCreateInfo.priority = 0;

    XrResult result = xrCreateActionSet(instance, &actionSetCreateInfo, &actionSet);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create action set: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return actionSet;
}

void destroyActionSet(XrActionSet actionSet)
{
    xrDestroyActionSet(actionSet);
}

XrAction createAction(XrActionSet actionSet, const char* name, XrActionType type)
{
    XrAction action;

    XrActionCreateInfo actionCreateInfo{};
    actionCreateInfo.type = XR_TYPE_ACTION_CREATE_INFO;
    strcpy(actionCreateInfo.actionName, name);
    strcpy(actionCreateInfo.localizedActionName, name);
    actionCreateInfo.actionType = type;

    XrResult result = xrCreateAction(actionSet, &actionCreateInfo, &action);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create action: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return action;
}

void destroyAction(XrAction action)
{
    xrDestroyAction(action);
}

XrSpace createActionSpace(XrSession session, XrAction action)
{
    XrSpace space;

    XrActionSpaceCreateInfo actionSpaceCreateInfo{};
    actionSpaceCreateInfo.type = XR_TYPE_ACTION_SPACE_CREATE_INFO;
    actionSpaceCreateInfo.poseInActionSpace = { { 0, 0, 0, 1 }, { 0, 0, 0 } };
    actionSpaceCreateInfo.action = action;

    XrResult result = xrCreateActionSpace(session, &actionSpaceCreateInfo, &space);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to create action space: " << result << endl;
        return XR_NULL_HANDLE;
    }

    return space;
}

void destroyActionSpace(XrSpace actionSpace)
{
    xrDestroySpace(actionSpace);
}

XrPath getPath(XrInstance instance, const char* name)
{
    XrPath path;

    XrResult result = xrStringToPath(instance, name, &path);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get path '" << name << "': " << result << endl;
        return XR_NULL_PATH;
    }

    return path;
}

void suggestBindings(
    XrInstance instance,
    XrAction leftHandAction,
    XrAction rightHandAction,
    XrAction leftGrabAction,
    XrAction rightGrabAction
)
{
    XrPath leftHandPath = getPath(instance, "/user/hand/left/input/grip/pose");
    XrPath rightHandPath = getPath(instance, "/user/hand/right/input/grip/pose");
    XrPath leftButtonPath = getPath(instance, "/user/hand/left/input/a/click");
    XrPath rightButtonPath = getPath(instance, "/user/hand/right/input/a/click");
    XrPath interactionProfilePath = getPath(instance, "/interaction_profiles/valve/index_controller");

    XrActionSuggestedBinding suggestedBindings[] = {
        { leftHandAction, leftHandPath },
        { rightHandAction, rightHandPath },
        { leftGrabAction, leftButtonPath },
        { rightGrabAction, rightButtonPath }
    };

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = interactionProfilePath;
    suggestedBinding.countSuggestedBindings = sizeof(suggestedBindings) / sizeof(XrActionSuggestedBinding);
    suggestedBinding.suggestedBindings = suggestedBindings;

    XrResult result = xrSuggestInteractionProfileBindings(instance, &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to suggest interaction profile bindings: " << result << endl;
    }
}

void attachActionSet(XrSession session, XrActionSet actionSet)
{
    XrSessionActionSetsAttachInfo actionSetsAttachInfo{};
    actionSetsAttachInfo.type = XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO;
    actionSetsAttachInfo.countActionSets = 1;
    actionSetsAttachInfo.actionSets = &actionSet;

    XrResult result = xrAttachSessionActionSets(session, &actionSetsAttachInfo);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to attach action set: " << result << endl;
    }
}

vector<XrSwapchainImageVulkanKHR> getSwapchainImages(XrSwapchain swapchain)
{
    uint32_t imageCount;

    XrResult result = xrEnumerateSwapchainImages(swapchain, 0, &imageCount, nullptr);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate swapchain images: " << result << endl;
        return {};
    }

    vector<XrSwapchainImageVulkanKHR> images(
        imageCount,
        { .type = XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR }
    );

    result = xrEnumerateSwapchainImages(swapchain, imageCount, &imageCount, (XrSwapchainImageBaseHeader*)images.data());

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to enumerate swapchain images: " << result << endl;
        return {};
    }

    return images;
}

VkInstance createVulkanInstance(XrGraphicsRequirementsVulkanKHR graphicsRequirements, set<string> instanceExtensions)
{
    VkInstance instance;

    size_t extensionCount = 1 + instanceExtensions.size();
    const char** extensionNames = new const char*[extensionCount];

    size_t i = 0;
    extensionNames[i] = vulkanExtensionNames[0];
    i++;

    for (const string& instanceExtension : instanceExtensions)
    {
        extensionNames[i] = instanceExtension.c_str();
        i++;
    }

    VkApplicationInfo applicationInfo{};
    applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    applicationInfo.pApplicationName = applicationName;
    applicationInfo.applicationVersion = VK_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    applicationInfo.pEngineName = applicationName;
    applicationInfo.engineVersion = VK_MAKE_VERSION(majorVersion, minorVersion, patchVersion);
    applicationInfo.apiVersion = graphicsRequirements.minApiVersionSupported;

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &applicationInfo;
    createInfo.enabledExtensionCount = extensionCount;
    createInfo.ppEnabledExtensionNames = extensionNames;
    createInfo.enabledLayerCount = 1;
    createInfo.ppEnabledLayerNames = vulkanLayerNames;

    VkResult result = vkCreateInstance(&createInfo, nullptr, &instance);

    delete[] extensionNames;

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan instance: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return instance;
}

void destroyVulkanInstance(VkInstance instance)
{
    vkDestroyInstance(instance, nullptr);
}

VkDebugUtilsMessengerEXT createVulkanDebugMessenger(VkInstance instance)
{
    VkDebugUtilsMessengerEXT debugMessenger;

    VkDebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo{};
    debugMessengerCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    debugMessengerCreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debugMessengerCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    debugMessengerCreateInfo.pfnUserCallback = handleVKError;

    auto vkCreateDebugUtilsMessengerEXT = (PFN_vkCreateDebugUtilsMessengerEXT)getVKFunction(instance, "vkCreateDebugUtilsMessengerEXT");

    VkResult result = vkCreateDebugUtilsMessengerEXT(instance, &debugMessengerCreateInfo, nullptr, &debugMessenger);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan debug messenger: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return debugMessenger;
}

void destroyVulkanDebugMessenger(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger)
{
    auto vkDestroyDebugUtilsMessengerEXT = (PFN_vkDestroyDebugUtilsMessengerEXT)getVKFunction(instance, "vkDestroyDebugUtilsMessengerEXT");

    vkDestroyDebugUtilsMessengerEXT(instance, debugMessenger, nullptr);
}

int32_t getDeviceQueueFamily(VkPhysicalDevice physicalDevice)
{
    int32_t graphicsQueueFamilyIndex = -1;

    uint32_t queueFamilyCount;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

    vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());

    for (int32_t i = 0; i < queueFamilyCount; i++)
    {
        if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            graphicsQueueFamilyIndex = i;
            break;
        }
    }

    if (graphicsQueueFamilyIndex == -1)
    {
        cerr << "No graphics queue found." << endl;
        return graphicsQueueFamilyIndex;
    }

    return graphicsQueueFamilyIndex;
}

tuple<VkDevice, VkQueue> createDevice(
    VkPhysicalDevice physicalDevice,
    int32_t graphicsQueueFamilyIndex,
    set<string> deviceExtensions
)
{
    VkDevice device;

    size_t extensionCount = deviceExtensions.size();
    const char** extensions = new const char*[extensionCount];

    size_t i = 0;
    for (const string& deviceExtension : deviceExtensions)
    {
        extensions[i] = deviceExtension.c_str();
        i++;
    }

    float priority = 1;

    VkDeviceQueueCreateInfo queueCreateInfo{};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = graphicsQueueFamilyIndex;
    queueCreateInfo.queueCount = 1;
    queueCreateInfo.pQueuePriorities = &priority;

    VkDeviceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.queueCreateInfoCount = 1;
    createInfo.pQueueCreateInfos = &queueCreateInfo;
    createInfo.enabledExtensionCount = extensionCount;
    createInfo.ppEnabledExtensionNames = extensions;

    VkResult result = vkCreateDevice(physicalDevice, &createInfo, nullptr, &device);

    delete[] extensions;

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan device: " << result << endl;
        return { VK_NULL_HANDLE, VK_NULL_HANDLE };
    }

    VkQueue queue;
    vkGetDeviceQueue(device, graphicsQueueFamilyIndex, 0, &queue);

    return { device, queue };
}

void destroyDevice(VkDevice device)
{
    vkDestroyDevice(device, nullptr);
}

VkRenderPass createRenderPass(VkDevice device)
{
    VkRenderPass renderPass;

    VkAttachmentDescription attachment{};
    attachment.format = VK_FORMAT_R8G8B8A8_SRGB;
    attachment.samples = VK_SAMPLE_COUNT_1_BIT;
    attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference attachmentRef{};
    attachmentRef.attachment = 0;
    attachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass{};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &attachmentRef;

    VkRenderPassCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    createInfo.flags = 0;
    createInfo.attachmentCount = 1;
    createInfo.pAttachments = &attachment;
    createInfo.subpassCount = 1;
    createInfo.pSubpasses = &subpass;

    VkResult result = vkCreateRenderPass(device, &createInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan render pass: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return renderPass;
}

void destroyRenderPass(VkDevice device, VkRenderPass renderPass)
{
    vkDestroyRenderPass(device, renderPass, nullptr);
}

VkCommandPool createCommandPool(VkDevice device, int32_t graphicsQueueFamilyIndex)
{
    VkCommandPool commandPool;

    VkCommandPoolCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    createInfo.queueFamilyIndex = graphicsQueueFamilyIndex;
    createInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    VkResult result = vkCreateCommandPool(device, &createInfo, nullptr, &commandPool);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to create Vulkan command pool: " << result << endl;
        return VK_NULL_HANDLE;
    }

    return commandPool;
}

void destroyCommandPool(VkDevice device, VkCommandPool commandPool)
{
    vkDestroyCommandPool(device, commandPool, nullptr);
}

bool renderEye(
    Swapchain* swapchain,
    const vector<SwapchainImage*>& images,
    XrView view,
    VkDevice device,
    VkQueue queue,
    VkRenderPass renderPass
)
{
    XrSwapchainImageAcquireInfo acquireImageInfo{};
    acquireImageInfo.type = XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO;

    uint32_t activeIndex;

    XrResult result = xrAcquireSwapchainImage(swapchain->swapchain, &acquireImageInfo, &activeIndex);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to acquire swapchain image: " << result << endl;
        return false;
    }

    XrSwapchainImageWaitInfo waitImageInfo{};
    waitImageInfo.type = XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO;
    waitImageInfo.timeout = numeric_limits<int64_t>::max();

    result = xrWaitSwapchainImage(swapchain->swapchain, &waitImageInfo);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to wait for swapchain image: " << result << endl;
        return false;
    }

    const SwapchainImage* image = images[activeIndex];

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    VkResult vkResult = vkBeginCommandBuffer(image->commandBuffer, &beginInfo);

    VkClearValue clearValue{};
    clearValue.color = { { 0.0f, 0.168f, 0.211f, 1.0f } };

    if (corrected)
    {
        clearValue.color.float32[0] = srgbToLinear(clearValue.color.float32[0]);
        clearValue.color.float32[1] = srgbToLinear(clearValue.color.float32[1]);
        clearValue.color.float32[2] = srgbToLinear(clearValue.color.float32[2]);
    }

    VkRenderPassBeginInfo beginRenderPassInfo{};
    beginRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginRenderPassInfo.renderPass = renderPass;
    beginRenderPassInfo.framebuffer = image->framebuffer;
    beginRenderPassInfo.renderArea = {
        { 0, 0 },
        { (uint32_t)swapchain->width, (uint32_t)swapchain->height }
    };
    beginRenderPassInfo.clearValueCount = 1;
    beginRenderPassInfo.pClearValues = &clearValue;

    vkCmdBeginRenderPass(image->commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    vkCmdEndRenderPass(image->commandBuffer);

    vkResult = vkEndCommandBuffer(image->commandBuffer);

    if (vkResult != VK_SUCCESS)
    {
        cerr << "Failed to end Vulkan command buffer: " << vkResult << endl;
        return false;
    }

    VkPipelineStageFlags stageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.pWaitDstStageMask = &stageMask;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &image->commandBuffer;

    vkResult = vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);

    if (vkResult != VK_SUCCESS)
    {
        cerr << "Failed to submit Vulkan command buffer: " << result << endl;
        return false;
    }

    XrSwapchainImageReleaseInfo releaseImageInfo{};
    releaseImageInfo.type = XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO;

    result = xrReleaseSwapchainImage(swapchain->swapchain, &releaseImageInfo);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to release swapchain image: " << result << endl;
        return false;
    }

    return true;
}

bool getActionBoolean(XrSession session, XrAction action)
{
    XrActionStateGetInfo getInfo{};
    getInfo.type = XR_TYPE_ACTION_STATE_GET_INFO;
    getInfo.action = action;

    XrActionStateBoolean state{};
    state.type = XR_TYPE_ACTION_STATE_BOOLEAN;

    XrResult result = xrGetActionStateBoolean(session, &getInfo, &state);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get boolean action state: " << result << endl;
        return false;
    }

    return state.currentState;
}

XrPosef getActionPose(XrSession session, XrAction action, XrSpace space, XrSpace roomSpace, XrTime predictedDisplayTime)
{
    XrPosef pose = {
        { 0, 0, 0, 1 },
        { 0, 0, 0 }
    };

    XrActionStateGetInfo getInfo{};
    getInfo.type = XR_TYPE_ACTION_STATE_GET_INFO;
    getInfo.action = action;

    XrActionStatePose state{};
    state.type = XR_TYPE_ACTION_STATE_POSE;

    XrResult result = xrGetActionStatePose(session, &getInfo, &state);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to get pose action state: " << result << endl;
        return pose;
    }

    XrSpaceLocation location{};
    location.type = XR_TYPE_SPACE_LOCATION;

    result = xrLocateSpace(space, roomSpace, predictedDisplayTime, &location);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to locate space: " << result << endl;
        return pose;
    }

    if (
        !(location.locationFlags & XR_SPACE_LOCATION_POSITION_VALID_BIT) ||
        !(location.locationFlags & XR_SPACE_LOCATION_ORIENTATION_TRACKED_BIT)
    )
    {
        cerr << "Received incomplete result when locating space." << endl;
        return pose;
    }

    return location.pose;
}

bool input(
    XrSession session,
    XrActionSet actionSet,
    XrSpace roomSpace,
    XrTime predictedDisplayTime,
    XrAction leftHandAction,
    XrAction rightHandAction,
    XrAction leftGrabAction,
    XrAction rightGrabAction,
    XrSpace leftHandSpace,
    XrSpace rightHandSpace
)
{
    XrActiveActionSet activeActionSet = {
        actionSet,
        XR_NULL_PATH
    };

    XrActionsSyncInfo syncInfo{};
    syncInfo.type = XR_TYPE_ACTIONS_SYNC_INFO;
    syncInfo.countActiveActionSets = 1;
    syncInfo.activeActionSets = &activeActionSet;

    XrResult result = xrSyncActions(session, &syncInfo);

    if (result == XR_SESSION_NOT_FOCUSED)
    {
        return true;
    }
    else if (result != XR_SUCCESS)
    {
        cerr << "Failed to synchronize actions: " << result << endl;
        return false;
    }

    XrPosef leftHand = getActionPose(session, leftHandAction, leftHandSpace, roomSpace, predictedDisplayTime);
    XrPosef rightHand = getActionPose(session, rightHandAction, rightHandSpace, roomSpace, predictedDisplayTime);

    bool leftGrab = getActionBoolean(session, leftGrabAction);
    bool rightGrab = getActionBoolean(session, rightGrabAction);

    if (leftGrab && !objectGrabbed && sqrt(pow(objectPos.x - leftHand.position.x, 2) + pow(objectPos.y - leftHand.position.y, 2) + pow(objectPos.z - leftHand.position.z, 2)) < grabDistance)
    {
        objectGrabbed = 1;
    }
    else if (!leftGrab && objectGrabbed == 1)
    {
        objectGrabbed = 0;
    }

    if (rightGrab && !objectGrabbed && sqrt(pow(objectPos.x - leftHand.position.x, 2) + pow(objectPos.y - leftHand.position.y, 2) + pow(objectPos.z - leftHand.position.z, 2)) < grabDistance)
    {
        objectGrabbed = 2;
    }
    else if (!rightGrab && objectGrabbed == 2)
    {
        objectGrabbed = 0;
    }

    switch (objectGrabbed)
    {
    case 0 :
        break;
    case 1 :
        objectPos = leftHand.position;
        break;
    case 2 :
        objectPos = rightHand.position;
        break;
    }

    return true;
}

bool render(
    XrSession session,
    Swapchain* swapchains[2],
    vector<SwapchainImage*> swapchainImages[2],
    XrSpace space,
    XrTime predictedDisplayTime,
    VkDevice device,
    VkQueue queue,
    VkRenderPass renderPass
)
{
    XrFrameBeginInfo beginFrameInfo{};
    beginFrameInfo.type = XR_TYPE_FRAME_BEGIN_INFO;

    XrResult result = xrBeginFrame(session, &beginFrameInfo);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to begin frame: " << result << endl;
        return false;
    }

    XrViewLocateInfo viewLocateInfo{};
    viewLocateInfo.type = XR_TYPE_VIEW_LOCATE_INFO;
    viewLocateInfo.viewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
    viewLocateInfo.displayTime = predictedDisplayTime;
    viewLocateInfo.space = space;

    XrViewState viewState{};
    viewState.type = XR_TYPE_VIEW_STATE;

    uint32_t viewCount = eyeCount;
    vector<XrView> views(
        viewCount,
        { .type = XR_TYPE_VIEW }
    );

    result = xrLocateViews(
        session,
        &viewLocateInfo,
        &viewState,
        viewCount,
        &viewCount,
        views.data()
    );

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to locate views: " << result << endl;
        return false;
    }

    for (size_t i = 0; i < eyeCount; i++)
    {
        renderEye(
            swapchains[i],
            swapchainImages[i],
            views[i],
            device,
            queue,
            renderPass
        );
    }

    XrCompositionLayerProjectionView projectedViews[2]{};

    for (size_t i = 0; i < eyeCount; i++)
    {
        projectedViews[i].type = XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW;
        projectedViews[i].pose = views[i].pose;
        projectedViews[i].fov = views[i].fov;
        projectedViews[i].subImage = {
            swapchains[i]->swapchain,
            {
                { 0, 0 },
                { (int32_t)swapchains[i]->width, (int32_t)swapchains[i]->height }
            },
            0
        };
    }

    XrCompositionLayerProjection layer{};
    layer.type = XR_TYPE_COMPOSITION_LAYER_PROJECTION;
    layer.space = space;
    layer.viewCount = eyeCount;
    layer.views = projectedViews;

    auto pLayer = (const XrCompositionLayerBaseHeader*)&layer;

    XrFrameEndInfo endFrameInfo{};
    endFrameInfo.type = XR_TYPE_FRAME_END_INFO;
    endFrameInfo.displayTime = predictedDisplayTime;
    endFrameInfo.environmentBlendMode = XR_ENVIRONMENT_BLEND_MODE_OPAQUE;
    endFrameInfo.layerCount = 1;
    endFrameInfo.layers = &pLayer;

    result = xrEndFrame(session, &endFrameInfo);

    if (result != XR_SUCCESS)
    {
        cerr << "Failed to end frame: " << result << endl;
        return false;
    }

    return true;
}

void onInterrupt(int)
{
    quit = true;
}

int main(int argc, char** argv)
{
    if (argc != 2 || (argv[1] != string("corrected") && argv[1] != string("uncorrected")))
    {
        cerr << "Usage: " << argv[0] << " corrected|uncorrected" << endl;
        return 1;
    }
    else if (argv[1] == string("corrected"))
    {
        corrected = true;
    }
    else
    {
        corrected = false;
    }

    XrInstance instance = createInstance();
    XrDebugUtilsMessengerEXT debugMessenger = createDebugMessenger(instance);
    XrSystemId system = getSystem(instance);

    XrGraphicsRequirementsVulkanKHR graphicsRequirements;
    set<string> instanceExtensions;
    tie(graphicsRequirements, instanceExtensions) = getVulkanInstanceRequirements(instance, system);
    VkInstance vulkanInstance = createVulkanInstance(graphicsRequirements, instanceExtensions);
    VkDebugUtilsMessengerEXT vulkanDebugMessenger = createVulkanDebugMessenger(vulkanInstance);

    VkPhysicalDevice physicalDevice;
    set<string> deviceExtensions;
    tie(physicalDevice, deviceExtensions) = getVulkanDeviceRequirements(instance, system, vulkanInstance);
    int32_t graphicsQueueFamilyIndex = getDeviceQueueFamily(physicalDevice);
    VkDevice device;
    VkQueue queue;
    tie(device, queue) = createDevice(physicalDevice, graphicsQueueFamilyIndex, deviceExtensions);

    VkRenderPass renderPass = createRenderPass(device);
    VkCommandPool commandPool = createCommandPool(device, graphicsQueueFamilyIndex);

    XrSession session = createSession(instance, system, vulkanInstance, physicalDevice, device, graphicsQueueFamilyIndex);

    Swapchain* swapchains[eyeCount];
    tie(swapchains[0], swapchains[1]) = createSwapchains(instance, system, session);

    vector<XrSwapchainImageVulkanKHR> swapchainImages[eyeCount];

    for (size_t i = 0; i < eyeCount; i++)
    {
        swapchainImages[i] = getSwapchainImages(swapchains[i]->swapchain);
    }

    vector<SwapchainImage*> wrappedSwapchainImages[eyeCount];

    for (size_t i = 0; i < eyeCount; i++)
    {
        wrappedSwapchainImages[i] = vector<SwapchainImage*>(swapchainImages[i].size(), nullptr);

        for (size_t j = 0; j < wrappedSwapchainImages[i].size(); j++)
        {
            wrappedSwapchainImages[i][j] = new SwapchainImage(
                physicalDevice,
                device,
                renderPass,
                commandPool,
                swapchains[i],
                swapchainImages[i][j]
            );
        }
    }

    XrSpace space = createSpace(session);

    XrActionSet actionSet = createActionSet(instance);

    XrAction leftHandAction = createAction(actionSet, "left-hand", XR_ACTION_TYPE_POSE_INPUT);
    XrAction rightHandAction = createAction(actionSet, "right-hand", XR_ACTION_TYPE_POSE_INPUT);
    XrAction leftGrabAction = createAction(actionSet, "left-grab", XR_ACTION_TYPE_BOOLEAN_INPUT);
    XrAction rightGrabAction = createAction(actionSet, "right-grab", XR_ACTION_TYPE_BOOLEAN_INPUT);

    XrSpace leftHandSpace = createActionSpace(session, leftHandAction);
    XrSpace rightHandSpace = createActionSpace(session, rightHandAction);

    suggestBindings(instance, leftHandAction, rightHandAction, leftGrabAction, rightGrabAction);

    attachActionSet(session, actionSet);

    signal(SIGINT, onInterrupt);

    bool running = false;
    while (!quit)
    {
        XrEventDataBuffer eventData{};
        eventData.type = XR_TYPE_EVENT_DATA_BUFFER;

        XrResult result = xrPollEvent(instance, &eventData);

        if (result == XR_EVENT_UNAVAILABLE)
        {
            if (running)
            {
                XrFrameWaitInfo frameWaitInfo{};
                frameWaitInfo.type = XR_TYPE_FRAME_WAIT_INFO;

                XrFrameState frameState{};
                frameState.type = XR_TYPE_FRAME_STATE;

                XrResult result = xrWaitFrame(session, &frameWaitInfo, &frameState);

                if (result != XR_SUCCESS)
                {
                    cerr << "Failed to wait for frame: " << result << endl;
                    break;
                }

                quit |= !input(
                    session,
                    actionSet,
                    space,
                    frameState.predictedDisplayTime,
                    leftHandAction,
                    rightHandAction,
                    leftGrabAction,
                    rightGrabAction,
                    leftHandSpace,
                    rightHandSpace
                );

                if (!frameState.shouldRender)
                {
                    continue;
                }

                quit |= !render(
                    session,
                    swapchains,
                    wrappedSwapchainImages,
                    space,
                    frameState.predictedDisplayTime,
                    device,
                    queue,
                    renderPass
                );
            }
        }
        else if (result != XR_SUCCESS)
        {
            cerr << "Failed to poll events: " << result << endl;
            break;
        }
        else
        {
            switch (eventData.type)
            {
            default :
                cerr << "Unknown event type received: " << eventData.type << endl;
                break;
            case XR_TYPE_EVENT_DATA_EVENTS_LOST :
                cerr << "Event queue overflowed and events were lost." << endl;
                break;
            case XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING :
                cout << "OpenXR instance is shutting down." << endl;
                quit = true;
                break;
            case XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED :
                cout << "The interaction profile has changed." << endl;
                break;
            case XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING :
                cout << "The reference space is changing." << endl;
                break;
            case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED :
            {
                auto event = (XrEventDataSessionStateChanged*)&eventData;

                switch (event->state)
                {
                case XR_SESSION_STATE_UNKNOWN :
                case XR_SESSION_STATE_MAX_ENUM :
                    cerr << "Unknown session state entered: " << event->state << endl;
                    break;
                case XR_SESSION_STATE_IDLE :
                    running = false;
                    break;
                case XR_SESSION_STATE_READY :
                {
                    XrSessionBeginInfo sessionBeginInfo{};
                    sessionBeginInfo.type = XR_TYPE_SESSION_BEGIN_INFO;
                    sessionBeginInfo.primaryViewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;

                    result = xrBeginSession(session, &sessionBeginInfo);

                    if (result != XR_SUCCESS)
                    {
                        cerr << "Failed to begin session: " << result << endl;
                    }

                    running = true;
                    break;
                }
                case XR_SESSION_STATE_SYNCHRONIZED :
                case XR_SESSION_STATE_VISIBLE :
                case XR_SESSION_STATE_FOCUSED :
                    running = true;
                    break;
                case XR_SESSION_STATE_STOPPING :
                    running = false;

                    result = xrEndSession(session);

                    if (result != XR_SUCCESS)
                    {
                        cerr << "Failed to end session: " << result << endl;
                    }
                    break;
                case XR_SESSION_STATE_LOSS_PENDING :
                    cout << "OpenXR session is shutting down." << endl;
                    quit = true;
                    break;
                case XR_SESSION_STATE_EXITING :
                    cout << "OpenXR runtime requested shutdown." << endl;
                    quit = true;
                    break;
                }
                break;
            }
            }
        }
    }

    VkResult result = vkDeviceWaitIdle(device);

    if (result != VK_SUCCESS)
    {
        cerr << "Failed to wait for device to idle: " << result << endl;
    }

    destroyActionSpace(rightHandSpace);
    destroyActionSpace(leftHandSpace);

    destroyAction(rightGrabAction);
    destroyAction(leftGrabAction);
    destroyAction(rightHandAction);
    destroyAction(leftHandAction);

    destroyActionSet(actionSet);

    destroySpace(space);

    for (size_t i = 0; i < eyeCount; i++)
    {
        for (size_t j = 0; j < wrappedSwapchainImages[i].size(); j++)
        {
            delete wrappedSwapchainImages[i][j];
        }
    }

    for (size_t i = 0; i < eyeCount; i++)
    {
        delete swapchains[i];
    }

    destroySession(session);

    destroyCommandPool(device, commandPool);
    destroyRenderPass(device, renderPass);

    destroyDevice(device);

    destroyVulkanDebugMessenger(vulkanInstance, vulkanDebugMessenger);
    destroyVulkanInstance(vulkanInstance);

    destroyDebugMessenger(instance, debugMessenger);
    destroyInstance(instance);

    return 0;
}
